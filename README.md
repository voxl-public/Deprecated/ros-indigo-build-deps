# ros-indigo-build-deps

This is a package that contains ROS indigo headers and cmake files to enable building ROS packages onboard VOXL. This is not necessary when building ROS nodes in voxl-emulator which already includes these files.